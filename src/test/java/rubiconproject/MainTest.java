package rubiconproject;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Before;

/**
 *
 * @author adolgarev
 */
public class MainTest {
    
    @Before
    public void before() throws IOException {
        Main main = new Main("input", "test_output");
        main.run();
    }
    
    @After
    public void after() throws IOException {
        Path path = FileSystems.getDefault().getPath("test_output");
        Files.delete(path);
    }

    @Test
    public void someTest() throws IOException {
        Path path = FileSystems.getDefault().getPath("test_output");
        List<String> lines = Files.readAllLines(path);
        assertEquals(2, lines.size());
        {
            String json = lines.get(0);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(json).getAsJsonObject();
            assertEquals("input1.csv", jsonObject.getAsJsonPrimitive("collectionId").getAsString());
            assertEquals(3, jsonObject.getAsJsonArray("sites").size());
            JsonObject site0 = jsonObject.getAsJsonArray("sites").get(0).getAsJsonObject();
            assertEquals(12000, site0.getAsJsonPrimitive("id").getAsInt());
            assertEquals("japan,travel", site0.getAsJsonPrimitive("keywords").getAsString());
        }
        {
            String json = lines.get(1);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(json).getAsJsonObject();
            assertEquals("input2.json", jsonObject.getAsJsonPrimitive("collectionId").getAsString());
            assertEquals(3, jsonObject.getAsJsonArray("sites").size());
            JsonObject site0 = jsonObject.getAsJsonArray("sites").get(0).getAsJsonObject();
            assertEquals(13000, site0.getAsJsonPrimitive("id").getAsInt());
            assertEquals("japan,travel", site0.getAsJsonPrimitive("keywords").getAsString());
        }
    }
}
