package rubiconproject.out;

import rubiconproject.Row;

/**
 *
 * @author adolgarev
 */
@FunctionalInterface
public interface Sink extends AutoCloseable {

    void processRow(Row row);

    @Override
    default void close() {
    }
}