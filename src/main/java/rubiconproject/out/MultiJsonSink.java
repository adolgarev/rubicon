package rubiconproject.out;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import rubiconproject.Row;

/**
 *
 * @author adolgarev
 */
public class MultiJsonSink implements Sink {
    
    private final File file;

    private OutputStreamWriter out;

    private JsonWriter jsonWriter;

    private final Gson gson;

    public MultiJsonSink(File file) throws UnsupportedEncodingException, FileNotFoundException, IOException {
        GsonBuilder builder = new GsonBuilder();
        BooleanToIntJsonSerializer booleanToIntJsonSerializer = new BooleanToIntJsonSerializer();
        builder.registerTypeAdapter(
                boolean.class, booleanToIntJsonSerializer);
        builder.registerTypeAdapter(
                Boolean.class, booleanToIntJsonSerializer);
        gson = builder.create();
        this.file = file;
        out = new OutputStreamWriter(
                new BufferedOutputStream(new FileOutputStream(file)), "UTF-8");
    }

    @Override
    public void processRow(Row row) {
        gson.toJson(row, Row.class, jsonWriter);
    }

    public void startNewCollectionWithId(String collectionId) throws IOException {
        jsonWriter = new JsonWriter(out);
        jsonWriter.beginObject();
        jsonWriter.name("collectionId").value(collectionId);
        jsonWriter.name("sites");
        jsonWriter.beginArray();
    }

    public void endCollection() throws IOException {
        jsonWriter.endArray();
        jsonWriter.endObject();
        jsonWriter.close();
        out = new OutputStreamWriter(
                new BufferedOutputStream(new FileOutputStream(file, true)), "UTF-8");
        out.append('\n');
    }
}
