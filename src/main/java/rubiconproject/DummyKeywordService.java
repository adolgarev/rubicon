package rubiconproject;

/**
 *
 * @author adolgarev
 */
public class DummyKeywordService implements KeywordService {

    @Override
    public String resolveKeywords(Object site) {
        return "japan,travel";
    }
    
}
