package rubiconproject;

import rubiconproject.in.Source;
import rubiconproject.out.Sink;
import rubiconproject.transform.InplaceRowTransformer;

/**
 *
 * @author adolgarev
 */
public class Pump {

    private Source source;

    private Sink sink;

    private InplaceRowTransformer transformer;

    public static class Engine {

        private final Pump pump;

        public Engine() {
            pump = new Pump();
        }

        public Engine from(Source source) {
            pump.source = source;
            return this;
        }

        public Engine to(Sink sink) {
            pump.sink = sink;
            return this;
        }

        public Engine transform(InplaceRowTransformer transformer) {
            pump.transformer = transformer;
            return this;
        }

        public Pump build() {
            if (pump.source == null || pump.sink == null) {
                throw new IllegalStateException();
            }
            return pump;
        }
    }

    public void start() {
        source.query().forEach(row -> {
            if (transformer != null) {
                transformer.execute(row);
            }
            sink.processRow(row);
        });
    }
}
