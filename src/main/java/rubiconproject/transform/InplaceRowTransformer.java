package rubiconproject.transform;

import rubiconproject.Row;

/**
 *
 * @author adolgarev
 */
@FunctionalInterface
public interface InplaceRowTransformer {

    void execute(Row input);

}