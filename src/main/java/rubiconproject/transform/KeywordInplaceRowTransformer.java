package rubiconproject.transform;

import rubiconproject.KeywordService;
import rubiconproject.Row;

/**
 *
 * @author adolgarev
 */
public class KeywordInplaceRowTransformer implements InplaceRowTransformer {
    
    private final KeywordService keywordService;

    public KeywordInplaceRowTransformer(KeywordService keywordService) {
        this.keywordService = keywordService;
    }

    @Override
    public void execute(Row input) {
        input.setKeywords(keywordService.resolveKeywords(input));
    }
    
}
