package rubiconproject.in;

import rubiconproject.Row;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author adolgarev
 */
public class CSVFileSource implements Source {

    private final List<Row> rows;

    public CSVFileSource(File file) throws FileNotFoundException, IOException {
        try (Reader in = new FileReader(file)) {
            Iterable<CSVRecord> records = CSVFormat.RFC4180.
                    withFirstRecordAsHeader().
                    withIgnoreEmptyLines().
                    parse(in);
            rows = new LinkedList<>();
            for (CSVRecord record : records) {
                Row row = new Row(
                        Integer.parseInt(record.get("id")),
                        record.get("name"),
                        Boolean.parseBoolean(record.get("is mobile")),
                        Integer.parseInt(record.get("score"))
                );
                rows.add(row);
            }
        }
    }

    @Override
    public Iterable<Row> query() {
        return rows;
    }
}
