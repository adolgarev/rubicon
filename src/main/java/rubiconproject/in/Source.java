package rubiconproject.in;

import rubiconproject.Row;

/**
 *
 * @author adolgarev
 */
@FunctionalInterface
public interface Source extends AutoCloseable {

    Iterable<Row> query();

    @Override
    default void close() {
    }
}