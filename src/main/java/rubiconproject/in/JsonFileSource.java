package rubiconproject.in;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import rubiconproject.Row;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author adolgarev
 */
public class JsonFileSource implements Source {

    private final List<Row> rows;

    public JsonFileSource(File file) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Type type = new TypeToken<List<Map<String, String>>>() {
        }.getType();
        try (Reader in = new FileReader(file)) {
            List<Map<String, String>> records = gson.fromJson(in, type);
            rows = new LinkedList<>();
            for (Map<String, String> record : records) {
                Row row = new Row(
                        Integer.parseInt(record.get("site_id")),
                        record.get("name"),
                        Boolean.parseBoolean(record.get("mobile")),
                        Integer.parseInt(record.get("score"))
                );
                rows.add(row);
            }
        }
    }

    @Override
    public Iterable<Row> query() {
        return rows;
    }

}
