package rubiconproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import rubiconproject.in.CSVFileSource;
import rubiconproject.in.JsonFileSource;
import rubiconproject.in.Source;
import rubiconproject.out.MultiJsonSink;
import rubiconproject.transform.KeywordInplaceRowTransformer;

/**
 *
 * @author adolgarev
 */
public class Main {

    private final String pathToDirectory;

    private final String outputFile;

    public Main(String pathToDirectory, String outputFile) {
        this.pathToDirectory = pathToDirectory;
        this.outputFile = outputFile;
    }

    public void run() throws FileNotFoundException, IOException {
        KeywordInplaceRowTransformer transformer = new KeywordInplaceRowTransformer(
                new DummyKeywordService());
        try (MultiJsonSink multiJsonSink = new MultiJsonSink(new File(outputFile))) {
            multiJsonSink.startNewCollectionWithId("input1.csv");
            try (Source source = new CSVFileSource(
                    new File(pathToDirectory, "input1.csv"))) {
                Pump pump = new Pump.Engine().
                        from(source).
                        transform(transformer).
                        to(multiJsonSink).
                        build();
                pump.start();
            }
            multiJsonSink.endCollection();
            multiJsonSink.startNewCollectionWithId("input2.json");
            try (Source source = new JsonFileSource(
                    new File(pathToDirectory, "input2.json"))) {
                Pump pump = new Pump.Engine().
                        from(source).
                        transform(transformer).
                        to(multiJsonSink).
                        build();
                pump.start();
            }
            multiJsonSink.endCollection();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length > 0) {
            new Main(args[0], args[1]).run();
            return;
        }
        throw new IllegalArgumentException();
    }
}
