package rubiconproject;

/**
 *
 * @author adolgarev
 */
public class Row {

    private final int id;
    
    private final String name;
    
    private final boolean mobile;
    
    private final int score;

    private String keywords;

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Row(int id, String name, boolean mobile, int score) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.score = score;
    }
    
}
