See https://gist.github.com/samtingleff/9a2935181210659eae56cffade3a25fd

Standard Java SE 8 Maven application.

To build: mvn install

To test: mvn test

To run: mvn exec:java
Can specify additional arguments, for instance
mvn exec:java -DoutputFile=ou1.json